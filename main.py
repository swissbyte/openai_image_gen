import glob
import io
import os
import shutil
import time
from PIL import Image
from dotenv import load_dotenv

import requests
from flask import Flask, render_template, request, jsonify, redirect
import openai
import base64
import json

load_dotenv()
app = Flask(__name__, template_folder=".")

# OpenAI API credentials
openai.api_key = os.getenv("OPENAI_API_KEY")
folder_path = "static/images"


def get_png_and_json_files(folder_path):
    png_files = glob.glob(os.path.join(folder_path, '*.png'))
    png_and_json_files = []

    for png_file in png_files:
        json_file = os.path.splitext(png_file)[0] + '.json'
        if os.path.exists(json_file):
            created_time = os.path.getctime(png_file)
            png_and_json_files.append(
                {"png": png_file, "json": json_file, "content": open_json(json_file), "created_time": created_time})

    sorted_files = sorted(png_and_json_files, key=lambda x: x['created_time'], reverse=True)
    return sorted_files


@app.route('/variant')
def create_variant():
    file_path = request.args.get('file')
    json_file = os.path.splitext(file_path)[0] + '.json'
    json_content = open_json(json_file)


    with open(file_path, mode="rb") as image_file:
        image_bytes = io.BytesIO(image_file.read())

    # Open the image using PIL
    image = Image.open(image_bytes)

    # Compress the image by approximately 20%
    quality = int(round(100 - (100 * 1)))
    output = io.BytesIO()
    image.save(output, format='png', quality=quality)
    compressed_image_bytes = output.getvalue()

    #NOT NEEDED. the function need the bytes not the base64 version
    #base64_img = base64.b64encode(compressed_image_bytes).decode('utf-8')

    # Bild-Variation erstellen
    response = openai.Image.create_variation(
        image=compressed_image_bytes,
        n=1,
        size="1024x1024",
        response_format="b64_json",
    )

    save_images_from_response(response, json_content['prompt'])
    return show_index()


@app.route('/archive')
def move_to_archive():
    file_path = request.args.get('file')

    filename = os.path.basename(file_path)
    json_filename = os.path.splitext(filename)[0] + ".json"
    archive_path = os.path.join("static", "images", "archive")
    if not os.path.exists(archive_path):
        os.makedirs(archive_path)
    new_file_path = os.path.join(archive_path, filename)
    new_json_path = os.path.join(archive_path, json_filename)
    shutil.move(file_path, new_file_path)
    shutil.move(file_path[:-4] + ".json", new_json_path)
    return show_index()


def get_last_json_content(folder_path):
    json_files = [f for f in os.listdir(folder_path) if f.endswith('.json')]
    if not json_files:
        return None
    latest_file = max(json_files, key=lambda f: os.path.getctime(os.path.join(folder_path, f)))
    with open(os.path.join(folder_path, latest_file), 'r') as file:
        content = json.load(file)
    return content


def open_json(json_file):
    with open(json_file, 'r') as file:
        content = json.load(file)
    return content


def get_latest_png_filename(folder_path):
    png_files = [f for f in os.listdir(folder_path) if f.endswith('.png')]
    if not png_files:
        return None
    latest_file = max(png_files, key=lambda x: os.path.getctime(os.path.join(folder_path, x)))
    return os.path.join(folder_path, latest_file)


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        prompt = request.form['prompt']
        generate_images(prompt)

    return show_index()


def show_index():
    image_list = get_png_and_json_files(folder_path)
    prompt = ""

    images = load_images()
    latest_prompt = get_last_json_content(folder_path)
    latest_filename = get_latest_png_filename(folder_path)
    return render_template('index.html', images=images, prompt=prompt, latest=latest_filename,
                           latest_prompt=latest_prompt, image_list=image_list)


@app.route('/image/<image_name>')
def get_image(image_name):
    with open(f'{folder_path}/{image_name}', 'rb') as f:
        image = f.read()
    return base64.b64encode(image).decode('ascii')


def generate_images(prompt):
    try:
        # Call OpenAI API to generate image(s)
        response = openai.Image.create(
            prompt=prompt,
            n=1,
            size="1024x1024",
            response_format="b64_json"
        )
        save_images_from_response(response, prompt)
        return {'status': 'success', 'data': response.data}

    except Exception as e:
        print(e)
        return {'status': 'error', 'message': 'Something went wrong.'}


def save_images_from_response(response, prompt):
    timestamp = str(int(time.time()))
    for data in response.data:
        image_data = base64.b64decode(data.b64_json)

        with open(f"{folder_path}/{timestamp}.png", 'wb') as f:
            f.write(image_data)

        data['prompt'] = prompt
        with open(f"{folder_path}/{timestamp}.json", mode="w", encoding="utf-8") as file:
            json.dump(data, file)


def save_images(images):
    # Save generated images to static/images directory
    for i, image in enumerate(images):
        with open(f'{folder_path}/{i}.jpg', 'wb') as f:
            f.write(base64.b64decode(image.split(",")[1]))

        # Save image prompt to JSON file
        with open(f'{folder_path}/{i}.json', 'w') as f:
            json.dump({'prompt': request.form['prompt']}, f)


def load_images():
    images = []
    # Load all saved images from static/images directory
    for i in range(4):
        try:
            with open(f'{folder_path}/{i}.jpg', 'rb') as f:
                image = base64.b64encode(f.read()).decode('ascii')
        except:
            image = None

        images.append({'name': f'{i}.jpg', 'image': image})

    return images


if __name__ == '__main__':
    app.run(debug=True)
